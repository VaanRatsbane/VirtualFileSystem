Simple virtual filesystem with folders, files, file sizes, and creation/modification dates. Also has cached folder/file counters per folder.

Initially created as a backbone for representing the inner file structure of VBF files in my other projects [VBFBrowser](https://gitlab.com/VaanRatsbane/VBFBrowser) and [DrakLab](https://gitlab.com/VaanRatsbane/DrakLab), I have matured it for more generalized purposes.

The VSystem can be initialized with an array of existing filepaths, or the files can be added later on. A filepath is a series of units seperated by forward slahes (/).
Both /folderA/folderB/file.foo and folderA/folderB/file.foo are valid paths.

The filesystem is composed by VUnits, that may be VFolders or VFiles. The filesystem's root is a folder with no name. The filesystem can enforce or ignore name casing.

The filesystem can be set with an initial datetime. All VUnits will derive their creation and modification dates from this setting.
Time can also be paused and resumed for the filesystem.