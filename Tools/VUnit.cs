﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualFileSystem
{

    public class VUnit
    {
        
        /// <summary>The name of this element.</summary>
        public string Name { get; private set; }

        /// <summary>The date when this element was created.</summary>
        public DateTime CreationDate { get { return FileSystem.CreationDate + (_creationDate - FileSystem._creationDateActual) - FileSystem.PausedTime; } }
        private DateTime _creationDate;

        /// <summary>The last date this element was modified.</summary>
        public DateTime LastModified { get { return FileSystem.CreationDate + (_lastModified - FileSystem._creationDateActual) - FileSystem.PausedTime; } }
        internal DateTime _lastModified;

        /// <summary>The system this element belongs to.</summary>
        public VSystem FileSystem { get; protected internal set; }

        /// <summary>This element's parent folder.</summary>
        public VFolder Parent { get; private set; }

        protected internal VUnit(VSystem system = null, VFolder parent = null)
        {
            FileSystem = system;
            Parent = parent;
            _creationDate = _lastModified = DateTime.UtcNow;
        }

        /// <summary>
        /// Moves the element.
        /// </summary>
        /// <param name="Destination">The destination folder.</param>
        /// <exception cref="InvalidOperationException"></exception>
        public void Move(VFolder Destination)
        {
            if (FileSystem is null && !(Destination.FileSystem is null))
            {
                throw new InvalidOperationException("You cannot move an item that does not belong to a filesystem to one that does.");
            }

            if (this is VFolder && (this as VFolder).Contains(Destination))
            {
                throw new InvalidOperationException("You cannot move this folder to a folder it contains.");
            }

            Parent.RemoveChild(this);
            Parent = Destination;
            Parent.AddChild(this);

            if(!FileSystem.Equals(Destination.FileSystem))
            {
                AlterFS(Destination.FileSystem);
            }
        }

        protected internal void AlterFS(VSystem fs)
        {
            FileSystem = fs;
            if(this is VFolder)
            {
                foreach(var child in (this as VFolder).GetChildren())
                {
                    child.AlterFS(fs);
                }
            }
        }

        /// <summary>
        /// Alters the element's name.
        /// </summary>
        /// <param name="NewName">The new name.</param>
        /// <exception cref="ArgumentException">Invalid filename.</exception>
        /// <exception cref="UnauthorizedAccessException">There already exists a file or folder with that name.</exception>
        public void ChangeName(string NewName)
        {
            if (String.IsNullOrWhiteSpace(NewName))
                throw new ArgumentException("Invalid filename.");

            if (String.IsNullOrWhiteSpace(Name) || Parent.UpdateChildName(Name, NewName))
            {
                Name = NewName;
                _lastModified = DateTime.UtcNow;
            }
            else
            {
                throw new UnauthorizedAccessException("There already exists a file or folder with that name.");
            }
        }
    }
}
