﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VirtualFileSystem
{
    public class VFolder : VUnit
    {

        /// <summary>The folder's name.</summary>
        public new string Name { get { return base.Name; } set { ChangeName(value); } }

        /// <summary>The folder's size in bytes.</summary>
        public ulong Size
        {
            get
            {
                ulong size = 0;
                foreach (var child in GetChildFolders())
                    size += child.Size;
                foreach (var child in GetChildFiles())
                    size += child.OriginalSize;
                return size;
            }
        }

        //Cached
        private CachedCounters counter;

        /// <summary>The amount of folders this folder contains.</summary>
        public ulong FolderCount
        {
            get
            {
                if (!counter.IsValid)
                    UpdateCounter(out ulong fo, out ulong fi);
                return counter.Folders;
            }
        }

        /// <summary>The amount of file this folder contains.</summary>
        public ulong FileCount
        {
            get
            {
                if (!counter.IsValid)
                    UpdateCounter(out ulong fo, out ulong fi);
                return counter.Files;
            }
        }

        private Dictionary<string, VUnit> children;

        protected internal VFolder(VSystem System) : base(System)
        {
            children = new Dictionary<string, VUnit>();
            counter = new CachedCounters();
        }

        /// <summary>
        /// Creates a new folder.
        /// </summary>
        /// <param name="System">The file system.</param>
        /// <param name="Parent">The folder's parent folder.</param>
        /// <param name="Name">The name of the folder.</param>
        public VFolder(VSystem System, VFolder Parent, string Name) : base(System, Parent)
        {
            children = new Dictionary<string, VUnit>();
            counter = new CachedCounters();
            this.Name = Name;
        }

        /// <summary>
        /// Adds a file given a path relative to the current folder.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="originalSize">(Optional) The file's size.</param>
        public void AddFile(string path, ulong originalSize)
        {
            path = path.TrimStart('/');
            var splices = path.Split('/');
            if (splices.Length == 1) //it's the file!
            {
                if(!FileSystem.IsCaseSensitive && HasFile(splices[0]))
                AddChild(new VFile(FileSystem, this, splices[0], originalSize));
            }
            else //it's a folder!
            {
                string restOfPath = "";
                for (int i = 1; i < splices.Length; i++)
                {
                    restOfPath += '/' + splices[i];
                }

                if (HasFolder(splices[0]))
                {
                    (GetChild(splices[0]) as VFolder).AddFile(restOfPath, originalSize);
                }
                else
                {
                    VFolder folder = new VFolder(FileSystem, this, splices[0]);
                    AddChild(folder);
                    folder.AddFile(restOfPath, originalSize);
                }
            }
        }

        protected internal void RemoveChild(VUnit unit)
        {
            children.Remove(unit.Name);
            Invalidate();
        }

        protected internal void AddChild(VUnit child)
        {
            children.Add(child.Name, child);
            if (child is VFile) AddedFile();
            else if (child is VFolder) AddedFolder();
        }

        /// <summary>
        /// Gets an element contained within this directory.
        /// </summary>
        /// <param name="childName">The name of the element.</param>
        public VUnit GetChild(string childName)
        {
            return children.ContainsKey(childName) ? children[childName] : null;
        }

        /// <summary>
        /// Gets an enumerator over the directory's children.
        /// </summary>
        public IEnumerable<VUnit> GetChildren()
        {
            return children.Values;
        }

        /// <summary>
        /// Gets an enumerator over the directory's folders.
        /// </summary>
        public IEnumerable<VFolder> GetChildFolders()
        {
            return children.Values.Where(c => c is VFolder).Cast<VFolder>();
        }

        /// <summary>
        /// Gets an enumerator over the directory's files.
        /// </summary>
        public IEnumerable<VFile> GetChildFiles()
        {
            return children.Values.Where(c => c is VFile).Cast<VFile>();
        }

        /// <summary>
        /// Verifies if the given element exists within the folder.
        /// </summary>
        /// <param name="unit">The element to search for.</param>
        /// <param name="recursive">Wether or not to iterate through all directory levels.</param>
        public bool Contains(VUnit unit, bool recursive = false)
        {
            foreach(var child in children.Values)
            {
                if (child.Equals(unit) || (recursive && child is VFolder && (child as VFolder).Contains(unit)))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Verifies if the directory contains a folder with the given name.
        /// </summary>
        /// <param name="FolderName">The folder to look for.</param>
        public bool HasFolder(string FolderName)
        {
            return children.Values.Any(c => c is VFolder && c.Name.Equals(FolderName));
        }

        /// <summary>
        /// Verifies if the directory contains a file with the given name.
        /// </summary>
        /// <param name="FileName">The file to look for.</param>
        public bool HasFile(string FileName)
        {
            return children.Values.Any(c => c is VFile && c.Name.Equals(FileName));
        }

        private void UpdateCounter(out ulong folders, out ulong files)
        {
            folders = 0;
            files = 0;
            foreach(var unit in children.Values)
            {
                if (unit is VFolder)
                {
                    (unit as VFolder).UpdateCounter(out ulong fo, out ulong fi);
                    folders += fo + 1;
                    files += fi;
                }
                else files++;
            }
            counter.Update(folders, files);
        }

        protected internal void AddedFile()
        {
            counter.Files++;
            if (!(Parent is null))
                Parent.AddedFile();
        }

        protected internal void AddedFolder()
        {
            counter.Folders++;
            if(!(Parent is null))
                Parent.AddedFolder();
        }

        protected internal void Invalidate()
        {
            counter.Invalidate();
            if(!(Parent is null))
                Parent.Invalidate();
        }

        /// <summary>
        /// Deletes an item with a given name.
        /// </summary>
        /// <param name="Name">The item to delete.</param>
        public void Delete(string Name)
        {
            if(children.Remove(Name, out var unit))
            {
                Invalidate();
            }
        }

        protected internal bool UpdateChildName(string Name, string NewName)
        {
            if (!children.ContainsKey(NewName) && children.Remove(Name, out var vunit))
            {
                children.Add(NewName, vunit);
                return true;
            }
            else return false;
        }

        protected internal sealed class CachedCounters
        {
            public bool IsValid { get; set; }
            public ulong Folders { get; protected internal set; }
            public ulong Files { get; protected internal set; }

            protected internal CachedCounters()
            {
                IsValid = false;
            }

            protected internal void Invalidate() => IsValid = false;

            protected internal void Update(ulong folders, ulong files)
            {
                Folders = folders;
                Files = files;
                IsValid = true;
            }
        }

    }
}
