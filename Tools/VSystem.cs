﻿using System;

namespace VirtualFileSystem
{
    public class VSystem
    {
        /// <summary>The name of the file system.</summary>
        public string SystemName { get; set; }

        /// <summary>The file system's root folder.</summary>
        public VFolder Root { get; private set; }

        /// <summary>The file system's root folder count.</summary>
        public ulong FolderCount { get { return Root.FolderCount; } }

        /// <summary>The file system's root file count.</summary>
        public ulong FileCount { get { return Root.FileCount; } }

        /// <summary>The moment the filesystem was created.</summary>
        public DateTime CreationDate { get; private set; }
        protected internal DateTime _creationDateActual;

        /// <summary>Wether or not to allow items in the same directory with the same name in different casing.</summary>
        public bool IsCaseSensitive { get; private set; }

        /// <summary>Overall how long the system has been suspended.</summary>
        public TimeSpan PausedTime {
            get
            {
                if (!(frozenDateTime is null))
                    return _pausedTime + (DateTime.UtcNow - frozenDateTime.Value);
                else
                    return _pausedTime;
            }
        }
        protected internal TimeSpan _pausedTime;

        private DateTime? frozenDateTime;

        /// <summary>
        /// Creates a file system.
        /// </summary>
        /// <param name="SystemName">The name of the file system.</param>
        /// <param name="Files">(Optional) The full path to each of the files to initialize the filesystem with.</param>
        /// <param name="FileSizes">(Optional) The size of each of the files in bytes.</param>
        /// <param name="CreationDate">(Optional) File system's creation date. All files' dates will derive from this.</param>
        /// <param name="isCaseSensitive">Wether or not to allow items in the same directory with the same name in different casing.</param>
        /// <exception cref="ArgumentException">Thrown if provided parameters are invalid.</exception>
        public VSystem(string SystemName, string[] Files = null, ulong[] FileSizes = null, DateTime? CreationDate = null, bool isCaseSensitive = false)
        {
            if (!(Files is null) && !(FileSizes is null) && Files.Length != FileSizes.Length)
                throw new ArgumentException("The amount of provided File Sizes must be the same as the amount of Files.");

            this.SystemName = SystemName;

            this.CreationDate = CreationDate ?? DateTime.UtcNow;
            _creationDateActual = DateTime.UtcNow;
            _pausedTime = TimeSpan.Zero;
            Root = new VFolder(this);
            IsCaseSensitive = isCaseSensitive;

            if(!(Files is null))
            for(int i = 0; i < Files.Length; i++)
            {
                var file = Files[i];
                var size = (FileSizes is null) ? 0 : FileSizes[i];
                Root.AddFile(file, size);
            }
        }

        /// <summary>
        /// Stops the passage of time for the filesystem.
        /// </summary>
        public void SuspendSystem()
        {
            if (frozenDateTime is null)
                frozenDateTime = DateTime.UtcNow;
        }

        /// <summary>
        /// Resumes the passage of time for the filesystem.
        /// </summary>
        public void ResumeSystem()
        {
            if (!(frozenDateTime is null))
            {
                _pausedTime += DateTime.UtcNow - frozenDateTime.Value;
                frozenDateTime = null;
            }
        }
    }
}
