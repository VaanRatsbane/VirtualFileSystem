﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualFileSystem
{
    /// <summary>
    /// A virtual file.
    /// </summary>
    public class VFile : VUnit
    {
        /// <summary>The file's name.</summary>
        public new string Name { get { return base.Name; } set { ChangeName(value); } }

        /// <summary>The file's size in bytes.</summary>
        public ulong OriginalSize { get {return _originalSize; } set { _lastModified = DateTime.UtcNow; OriginalSize = value; } }
        private ulong _originalSize;

        /// <summary>
        /// Instantiates a new file.
        /// </summary>
        /// <param name="name">The file's name.</param>
        /// <param name="originalSize">The file's size in bytes.</param>
        public VFile(string name, ulong originalSize)
        {
            OriginalSize = originalSize;
            Name = name;
        }

        protected internal VFile(VSystem system, VFolder parent, string name, ulong originalSize) : base(system, parent)
        {
            OriginalSize = originalSize;
            Name = name;
        }
    }
}
